#==============================================================================
# Generate database on public water supply (Wasserstatistik)
# Author: Greta Sundermann                                                                                 
# Date: 31.08.2021                                                                                        
#                                                                                    
# Input data set 1:  4193_Wasser_WVU_2007.dta
# Input data set 2:  4193_Wasser_WVU_2010.dta
# Input data set 3:  4193_Wasser_WVU_2013.dta
# Input data set 4:  4193_Wasser_WVU_2016.dta

# Input data set 5:  4193_Wasser_versGemeinde_2007.dta
# Input data set 6:  4193_Wasser_versGemeinde_2010.dta
# Input data set 7:  4193_Wasser_versGemeinde_2013.dta
# Input data set 8:  4193_Wasser_versGemeinde_2016.dta

# Input data set 9:  4193_Wasser_Anlage_2007.dta
# Input data set 10: 4193_Wasser_Anlage_2010.dta
# Input data set 11: 4193_Wasser_Anlage_2013.dta
# Input data set 12: 4193_Wasser_Anlage_2016.dta

# Output data set 1: 4193_Wasser_WVU_2007-2016.rds
# Output data set 2: 4193_Wasser_WVG_2007-2016.rds 
# Output data set 3: 4193_Wasser_WGA_2007-2016.rds 
# Output data set 4: 4193_Wasser_wvu_wvg_wga_2007-2016.rds 
#==============================================================================

#==============================================================================
# 1. load packages
#==============================================================================
library(haven)
library(dplyr)
library(ggplot2)

#==============================================================================
# 2. data preparation
#==============================================================================

# load the data sets
# ------------------
# Wasserstatistik: Wasserversorgungsunternehmen
data_wvu_2007 <- read_dta(paste0(datenpfad,"/","4193_Wasser_WVU_2007.dta", sep=""))
data_wvu_2010 <- read_dta(paste0(datenpfad,"/","4193_Wasser_WVU_2010.dta", sep=""))
data_wvu_2013 <- read_dta(paste0(datenpfad,"/","4193_Wasser_WVU_2013.dta", sep=""))
data_wvu_2016 <- read_dta(paste0(datenpfad,"/","4193_Wasser_WVU_2016.dta", sep=""))

# Wasserstatistik: versorgte Gemeinden
data_wvg_2007 <- read_dta(paste0(datenpfad,"/","4193_Wasser_versGemeinde_2007.dta", sep="")) 
data_wvg_2010 <- read_dta(paste0(datenpfad,"/","4193_Wasser_versGemeinde_2010.dta", sep=""))
data_wvg_2013 <- read_dta(paste0(datenpfad,"/","4193_Wasser_versGemeinde_2013.dta", sep=""))
data_wvg_2016 <- read_dta(paste0(datenpfad,"/","4193_Wasser_versGemeinde_2016.dta", sep=""))

# Wasserstatistik: Wassergewinnungsanlagen
data_wga_2007 <- read_dta(paste0(datenpfad,"/","4193_Wasser_Anlage_2007.dta", sep=""))
data_wga_2010 <- read_dta(paste0(datenpfad,"/","4193_Wasser_Anlage_2010.dta", sep=""))
data_wga_2013 <- read_dta(paste0(datenpfad,"/","4193_Wasser_Anlage_2013.dta", sep=""))
data_wga_2016 <- read_dta(paste0(datenpfad,"/","4193_Wasser_Anlage_2016.dta", sep=""))

#===================================================================
# 3.1 preparation of individual statistics 
#===================================================================
#===================================================================
# 3.1.1 individual statistic: water supplying companies
#===================================================================

# how many observations are there per year?
# ----------------------------------------
dim(data_wvu_2007)
dim(data_wvu_2010)
dim(data_wvu_2013)
dim(data_wvu_2016)

# what are the names of the variables?
# ----------------------------------------
names.wvu.2007 <- unlist(dimnames(data_wvu_2007)[2])
names.wvu.2007
names.wvu.2010 <- unlist(dimnames(data_wvu_2010)[2])
names.wvu.2010
names.wvu.2013 <- unlist(dimnames(data_wvu_2013)[2])
names.wvu.2013
names.wvu.2016 <- unlist(dimnames(data_wvu_2016)[2])
names.wvu.2016

# add a prefix for identification
# ------------------------------------------------------------
# background: the names of the variables are identical across the different
# individual statistics (EF1, EF2, ...)
names.wvu.2007.new <- paste0("wvu_",names.wvu.2007)
colnames(data_wvu_2007) <- names.wvu.2007.new
names.wvu.2010.new <- paste0("wvu_",names.wvu.2010)
colnames(data_wvu_2010) <- names.wvu.2010.new
names.wvu.2013.new <- paste0("wvu_",names.wvu.2013)
colnames(data_wvu_2013) <- names.wvu.2013.new
names.wvu.2016.new <- paste0("wvu_",names.wvu.2016)
colnames(data_wvu_2016) <- names.wvu.2016.new


# variable names are different for 2010 and 2013/2016, adapt the names
# ---------------------------------------------------------------------
# In 2010, size class variables are included that do not exist before and after. 
# Delete them completely from the panel, since their classification was unclear 
# anyway.
names(data_wvu_2010)
names(data_wvu_2013)
names(data_wvu_2016)

# remove the size class variables.
data_wvu1_2010 <- subset(data_wvu_2010, 
                         select=-c(wvu_EF12,wvu_EF19,wvu_EF38,wvu_EF39))

# align the variable names to 2013/2016.
data_wvu2_2010 <- data_wvu1_2010 %>% rename(wvu_EF12 = wvu_EF13,
                                            wvu_EF13 = wvu_EF14,
                                            wvu_EF14 = wvu_EF15,
                                            wvu_EF15 = wvu_EF16,
                                            wvu_EF16 = wvu_EF17,
                                            wvu_EF17 = wvu_EF18,
                                            wvu_EF18 = wvu_EF20,
                                            wvu_EF19 = wvu_EF21,
                                            wvu_EF20 = wvu_EF22,
                                            wvu_EF21 = wvu_EF23,
                                            wvu_EF22 = wvu_EF24,
                                            wvu_EF23 = wvu_EF25,
                                            wvu_EF24 = wvu_EF26,
                                            wvu_EF25 = wvu_EF27,
                                            wvu_EF26 = wvu_EF28,
                                            wvu_EF27 = wvu_EF29,
                                            wvu_EF28 = wvu_EF30,
                                            wvu_EF29 = wvu_EF31,
                                            wvu_EF30 = wvu_EF32,
                                            wvu_EF31 = wvu_EF33,
                                            wvu_EF32 = wvu_EF34,
                                            wvu_EF35U1_alt = wvu_EF35U1,
                                            wvu_EF35U2_alt = wvu_EF35U2,
                                            wvu_EF33  = wvu_EF36,
                                            wvu_EF34 =  wvu_EF37,
                                            wvu_EF35 =  wvu_EF40)

# create empty variables in the dataset '2013' and '2016' for those that are no 
# longer continued.
# ------------------------------------------------------------------------------
data_wvu_2013$wvu_EF35U1_alt <- NA
data_wvu_2013$wvu_EF35U2_alt <- NA
data_wvu_2016$wvu_EF35U1_alt <- NA
data_wvu_2016$wvu_EF35U2_alt <- NA


# are all variable names now identical?
# ---------------------------------------
setdiff(names(data_wvu2_2010),names(data_wvu_2013))
setdiff(names(data_wvu_2013),names(data_wvu2_2010))
setdiff(names(data_wvu2_2010),names(data_wvu_2016))
setdiff(names(data_wvu_2016),names(data_wvu2_2010))


# merge the three survey years (2010, 2013, 2016) together.
# ------------------------------------------------------------
data_wvu_1016 <- rbind(data_wvu2_2010,data_wvu_2013, data_wvu_2016)

# create an indicator 'wvu' to show that the observations come from the 
# individual statistic 'water utilities'.
# --------------
data_wvu_2007$wvu <- 1
data_wvu_1016$wvu <- 1
dim(data_wvu_2007)
dim(data_wvu_1016)

#===================================================================
# 3.1.2 individual statistic: supplied municipalities
#===================================================================
# how many observations are there?
# ----------------------------------------
dim(data_wvg_2007)
dim(data_wvg_2010)
dim(data_wvg_2013)
dim(data_wvg_2016)

# what are the names of the variables?
# ------------------------------------
names.wvg.2007 <- unlist(dimnames(data_wvg_2007)[2])
names.wvg.2007
names.wvg.2010 <- unlist(dimnames(data_wvg_2010)[2])
names.wvg.2010
names.wvg.2013 <- unlist(dimnames(data_wvg_2013)[2])
names.wvg.2013
names.wvg.2016 <- unlist(dimnames(data_wvg_2016)[2])
names.wvg.2016

# add a prefix
# -------------
names.wvg.2007.new <- paste0("wvg_",names.wvg.2007)
colnames(data_wvg_2007) <- names.wvg.2007.new
names.wvg.2010.new <- paste0("wvg_",names.wvg.2010)
colnames(data_wvg_2010) <- names.wvg.2010.new
names.wvg.2013.new <- paste0("wvg_",names.wvg.2013)
colnames(data_wvg_2013) <- names.wvg.2013.new
names.wvg.2016.new <- paste0("wvg_",names.wvg.2016)
colnames(data_wvg_2016) <- names.wvg.2016.new

# merge the years 2010, 2013 and 2016, which should be identical in structure 
# ----------------------------------------------------------------------------
data_wvg_1016 <- rbind(data_wvg_2010,data_wvg_2013, data_wvg_2016)
dim(data_wvg_1016)

# create an indicator 'wvg' to show that the observations come from the 
# individual statistic 'supplied municipalities'.
# ----------------------------------------------------------------------------
data_wvg_2007$wvg <- 1
data_wvg_1016$wvg <- 1
dim(data_wvg_2007)
dim(data_wvg_1016)

#===================================================================
# 3.1.3 individual statistic: water abstraction plants
#===================================================================
# how many observations are there per year?
# ----------------------------------------
dim(data_wga_2007)
dim(data_wga_2010)
dim(data_wga_2013)
dim(data_wga_2016)

# what are the names of the variables?
# ------------------------------------------------------
names.wga.2007 <- unlist(dimnames(data_wga_2007)[2])
names.wga.2007
names.wga.2010 <- unlist(dimnames(data_wga_2010)[2])
names.wga.2010
names.wga.2013 <- unlist(dimnames(data_wga_2013)[2])
names.wga.2013
names.wga.2016 <- unlist(dimnames(data_wga_2016)[2])
names.wga.2016

# add a prefix 'wga' to the variables for identification
# ------------------------------------------------------------
names.wga.2007.new <- paste0("wga_",names.wga.2007)
colnames(data_wga_2007) <- names.wga.2007.new
names.wga.2010.new <- paste0("wga_",names.wga.2010)
colnames(data_wga_2010) <- names.wga.2010.new
names.wga.2013.new <- paste0("wga_",names.wga.2013)
colnames(data_wga_2013) <- names.wga.2013.new
names.wga.2016.new <- paste0("wga_",names.wga.2016)
colnames(data_wga_2016) <- names.wga.2016.new

# Merge the years 2010, 2013 and 2016, which should be identical in structure
# ---------------------------------------------------------------------------
data_wga_1016 <- rbind(data_wga_2010,data_wga_2013,data_wga_2016)
dim(data_wga_1016)

# add id 'wga'
# --------------
data_wga_2007$wga <- 1
data_wga_1016$wga <- 1
dim(data_wga_2007)
dim(data_wga_1016)

# clear environment
# -----------------
rm(data_wvu_2010,data_wvu_2013,data_wvu_2016, 
   data_wvg_2010,data_wvg_2013,data_wvg_2016,
   data_wga_2010,data_wga_2013,data_wga_2016)

#===================================================================
# 3.2 harmonize 2007 with the years 2010-2016
#===================================================================
# the questionnaire has been slightly changed from 2010 and some variables have 
# been inserted (esp.river basins), which has shifted the sequential naming 
# (EF2,EF3,...) of many variables has changed.

# the statistics for the years from 2010 onward are therefore more detailed, 
# however, many newly added variables can also be calculated for the year 2007 
# from the existing variables.

# therefore, align the year 2007 with the years 2010, 2013 and 2016
# variable names are based on 2016
#===================================================================
# 3.1.1 processing of water supplying companies
#===================================================================
names(data_wvu_2007)
names(data_wvu_1016)

# align the variable names from 2007 to 2010-2016.
# ---------------------------------------------------------
data_wvu2_2007 <- data_wvu_2007 %>% 
  rename(wvu_EF2_alt = wvu_EF2,
         wvu_EF2 = wvu_EF3,
         wvu_EF2U1= wvu_EF3U1,
         wvu_EF2U2= wvu_EF3U2,
         wvu_EF2U3= wvu_EF3U3,
         wvu_EF3U4_alt= wvu_EF3U4,
         wvu_EF3 = wvu_EF4,
         wvu_EF3U1= wvu_EF4U1,
         wvu_EF3U2= wvu_EF4U2,
         wvu_EF3U3= wvu_EF4U3,
         wvu_EF21= wvu_EF18,
         wvu_EF22= wvu_EF19,
         wvu_EF23= wvu_EF20, 
         wvu_EF24= wvu_EF21,
         wvu_EF25= wvu_EF22,
         wvu_EF26= wvu_EF23,
         wvu_EF27= wvu_EF24,
         wvu_EF28= wvu_EF25,
         wvu_EF29= wvu_EF26,
         wvu_EF30= wvu_EF27,
         wvu_EF31= wvu_EF28,
         wvu_EF32= wvu_EF29,
         wvu_EF35U2_alt= wvu_EF30,
         wvu_EF33= wvu_EF31,
         wvu_EF34= wvu_EF32,
         wvu_EF35=wvu_EF33)

# following variables remain unchanged
# --------------------------------------
# wvu_EF5, wvu_EF6, wvu_EF7, wvu_EF8, wvu_EF9, wvu_EF10, wvu_EF11, wvu_EF12, wvu_EF13, wvu_EF14
# wvu_EF15, wvu_EF16, wvu_EF17

# create the new variables that can be constructed from the previous variables.
# -----------------------------------------------------------------------------
# EF18: Versorgte Einwohner innerhalb des Bundeslandes
# EF19: Wasserabgabe an Letztverbraucher innerhalb des Bundeslandes
# EF20: Wasserabgabe an Haushalte (inkl. Kleinbetriebe) innerhalb des Bundeslandes
data_wvu2_2007$wvu_EF18 <- data_wvu2_2007$wvu_EF24-data_wvu2_2007$wvu_EF21
data_wvu2_2007$wvu_EF19 <- data_wvu2_2007$wvu_EF25-data_wvu2_2007$wvu_EF22
data_wvu2_2007$wvu_EF20 <- data_wvu2_2007$wvu_EF26-data_wvu2_2007$wvu_EF23

# create empty variables for those variables from 2010 that cannot be derived.
# ----------------------------------------------------------------------------
# river basins
data_wvu2_2007$wvu_EF4U1 <- NA
data_wvu2_2007$wvu_EF4U2 <- NA
data_wvu2_2007$wvu_EF4U3 <- NA

# Verbandsschlüssel (9-Steller): Achtung, Variable EF3U4 aus dem Jahr 2007 entspricht 
# nicht dem Verbandschlüssel (9-Steller), sondern dem AGS (8-Steller).
data_wvu2_2007$wvu_EF2U4 <- NA
# Regionalschlüssel (12-stellig)
data_wvu2_2007$wvu_EF2U5 <- NA
#  Vorzeichen des Messfehlers
data_wvu2_2007$wvu_EF35U1_alt <- NA

# add empty columns leading those variables that were not continued from 2010.
# -----------------------------------------------------------------------------
data_wvu_1016$wvu_EF2_alt <- NA
data_wvu_1016$wvu_EF3U4_alt <- NA

# are all variable names now identical?
# ---------------------------------------
setdiff(names(data_wvu2_2007),names(data_wvu_1016))
setdiff(names(data_wvu_1016),names(data_wvu2_2007))

# add both periods together
# --------------------------------
data_wvu_0716 <- rbind(data_wvu2_2007,data_wvu_1016)
names(data_wvu_0716)

data_wvu_0716 <- data_wvu_0716 %>% rename(jahr = wvu_jahr,
                                          EF1 = wvu_EF1) %>% 
                                   select(EF1, jahr, wvu, everything())

# how many observations are there per year?
addmargins(table(data_wvu_0716$jahr,useNA="ifany"))

# export the data set
# ------------------------
# Individual statistics 'Water supplying companies' for the years 2007, 2010, 
# 2013 and 2016

# Remove all empty fields, because Stata does not handle empty strings
data_wvu_0716[data_wvu_0716=="."] <- NA
data_wvu_0716[data_wvu_0716==""] <- NA

saveRDS(data_wvu_0716,paste0(neudatenpfad,"/","4193_Wasser_WVU_2007-2016.rds"))

#===================================================================
# 3.1.2 processing of supplied municipalities
#===================================================================
names(data_wvg_2007)
names(data_wvg_1016)

# align the variable names from 2007 to 2010-2016
# ------------------------------------------------
data_wvg2_2007 <- data_wvg_2007 %>% rename(wvg_EF5 = wvg_EF4,
                                           wvg_EF6 = wvg_EF5, 
                                           wvg_EF7 = wvg_EF6,
                                           wvg_EF9 = wvg_EF7)

# following variables remain unchanged
# --------------------------------------------------------
# wvg_EF2, wvg_EF2U1, wvg_EF2U2, wvg_EF2U3, wvg_EF2U4, wvg_EF3U1, wvg_EF3U2, wvg_EF3U3

# create the new variables that can be constructed from the previous variables.
# ------------------------------------------------------------------------------
# river basin
data_wvg2_2007$wvg_EF4U1 <- NA
data_wvg2_2007$wvg_EF4U2 <- NA
data_wvg2_2007$wvg_EF4U3 <- NA
# Regionalschlüssel (12-stellig)
data_wvg2_2007$wvg_EF2U5 <- NA

# supplied citizens
data_wvg2_2007$wvg_EF8 <- NA

# are now all variables identical?
# ---------------------------------------
setdiff(names(data_wvg2_2007),names(data_wvg_1016))
setdiff(names(data_wvg_1016),names(data_wvg2_2007))

# merge both time periods
# --------------------------------
data_wvg_0716 <- rbind(data_wvg2_2007,data_wvg_1016)

data_wvg_0716 = data_wvg_0716 %>% rename(jahr = wvg_jahr,
                                         EF1 = wvg_EF1) %>% 
                                  select(EF1, jahr, wvg, wvg_EF2, everything())
dim(data_wvg_0716)

# how many observations are there per year?
# ------------------------------------------
addmargins(table(data_wvg_0716$jahr,useNA="ifany"))

# warning, the number of observations is not at the municipality level. 
# If a municipality is served by more than one WVU, there are multiple lines.

# remove all empty fields, because Stata does not handle empty strings
# --------------------------------------------------------------------
data_wvg_0716[data_wvg_0716=="."] <- NA
data_wvg_0716[data_wvg_0716==""] <- NA

# export data
# -----------
saveRDS(data_wvg_0716,paste0(neudatenpfad,"/","4193_Wasser_WVG_2007-2016.rds",sep=""))

#===================================================================
# 3.1.3 processing of water abstraction plants
#===================================================================
names(data_wga_2007)
names(data_wga_1016)

# align the variable names from 2007 to 2010-2016
# -----------------------------------------------
data_wga2_2007 <- data_wga_2007 %>% rename(wga_EF6 = wga_EF5,
                                           wga_EF7 = wga_EF6, 
                                           wga_EF8 = wga_EF7,
                                           wga_EF9 = wga_EF8,
                                           wga_EF10 = wga_EF9,
                                           wga_EF11 = wga_EF10,
                                           wga_EF12 = wga_EF11) 

# following variables remain unchanged
# --------------------------------------
# wga_EF2, wga_EF3U1, wga_EF3U2, wga_EF3U3,wga_EF3U4, wga_EF4U1, wga_EF4U2, wga_EF4U3

# create the new variables that can be constructed from the previous variables
# ------------------------------------------------------------------------------
# river basin
data_wga2_2007$wga_EF5U1 <- NA
data_wga2_2007$wga_EF5U2 <- NA
data_wga2_2007$wga_EF5U3 <- NA
# Regionalschlüssel (12-stellig)
data_wga2_2007$wga_EF3U5 <- NA

# are now all variables identical?
# ---------------------------------------
setdiff(names(data_wga2_2007),names(data_wga_1016))
setdiff(names(data_wga_1016),names(data_wga2_2007))

# merge both time periods
# --------------------------------
data_wga_0716 <- rbind(data_wga2_2007,data_wga_1016)
names(data_wga_0716)

data_wga_0716 = data_wga_0716  %>%   rename(jahr = wga_jahr, EF1 = wga_EF1) %>% 
                                     select(EF1, jahr, wga, everything())

dim(data_wga_0716)

# how many observations are there per year?
# ------------------------------------------
addmargins(table(data_wga_0716$jahr,useNA="ifany"))

# warning, the number of observations is not at the facility level. 
# If a filing system supplies several WVU, there will be several lines.

# remove all empty fields, because Stata does not handle empty strings
# --------------------------------------------------------------------
data_wga_0716[data_wga_0716=="."] <- NA
data_wga_0716[data_wga_0716==""] <- NA

# export data
# ------------
saveRDS(data_wga_0716,paste0(neudatenpfad,"/","4193_Wasser_WGA_2007-2016.rds",sep=""))

#===================================================================
# 3.2 merge individual statistics 
#===================================================================
#===================================================================
# 3.2.1 Connect the communities served with the water abstraction plants
#===================================================================
# merge the water production facilities with the communities served. 
# from each data set, retain any observations that could not be merged.
data_w1_0716 <- merge(data_wga_0716,data_wvg_0716,by=c("EF1","jahr"),all=TRUE)

# there should be 24 + 21 - 1xID - 1xJahr = 43 columns
dim(data_w1_0716)

# How many observations per year come from the water abstraction plant dataset. 
# (wga==1) and how many from the 'supplied municipality' dataset (wvg==1)?
addmargins(table(data_w1_0716$wga,data_w1_0716$jahr,useNA="ifany"))
addmargins(table(data_w1_0716$wvg,data_w1_0716$jahr,useNA="ifany"))

# for how many observations are both the wga data and the wvg data available?
addmargins(table(data_w1_0716$jahr[data_w1_0716$wvg==1 & data_w1_0716$wga==1]))

# add a prefix
data_wvg_wga = data_w1_0716 %>% select(EF1, jahr, wvg, wga, wvg_EF2, everything())
dim(data_wvg_wga)

data_wvg_wga$wvg[is.na(data_wvg_wga$wvg)==TRUE] <- 0
data_wvg_wga$wga[is.na(data_wvg_wga$wga)==TRUE] <- 0

# export data
# ----------------------
# this is a dataset that combines the individual statistics 'supplied 
# municipality' and 'water abstraction plants' combined and covers the years 
# 2007-2016.

# remove all empty fields, because Stata does not handle empty strings
# --------------------------------------------------------------------
data_wvg_wga[data_wvg_wga=="."] <- NA
data_wvg_wga[data_wvg_wga==""] <- NA

# export data
# --------------
saveRDS(data_wvg_wga,paste0(neudatenpfad,"/","4193_Wasser_wvg_wga_2007-2016.rds"))

#===================================================================
# 3.2.2 Connect the communities served and water abstraction plants
#       with water supplying companies
#===================================================================

# merge the water abstraction plants and supplied municipalities with the water 
# suppliers. retain from each data set any observations that could not be merged.
data_w2_0716 <- merge(data_w1_0716,data_wvu_0716,by=c("EF1","jahr"),all=TRUE)

# there should be 51 + 21 + 24 - 2xID - 2xJahr = 92 columns
dim(data_w2_0716)

# check numbers
addmargins(table(data_w2_0716$wvu,data_w2_0716$jahr,useNA="ifany"))
addmargins(table(data_w2_0716$wvg,data_w2_0716$jahr,useNA="ifany"))
addmargins(table(data_w2_0716$wga,data_w2_0716$jahr,useNA="ifany"))

# note: A company may operate in more than one community and a community may be 
# served by more than one companies. therefore, the assignment is not unique and
# the number of rows (e.g. wvu==1) may differ from the sum of the individual 
# statistics.

# case 1: one company active in several communities
# ---------------------------------------------------
# if a company serves 2 communities, there are 2 lines per year for that company,
# one for each community. If one wants to know how much the company has done in 
# total, one must sum annually across all the municipalities it serves (e.g., 
# water delivery). this corresponds to the structure of the company-operation 
# level in the energy panel, where the municipalities behave like the companies. 
# behave like the companies.

# case 2: a municipality is supplied by several companies
# ------------------------------------------------------------
# if a community is served by 2 companies, the community variables are specified
# specifically for each company (e.g., residents served in the community by that 
# company). background: the questionnaire is completed by the companies and not 
# by the municipalities. the companies only know how much they themselves 
# provide, but not what the others are are doing. If one wants to aggregate the 
# municipality variables at the municipality level (e.g., total p.e. served),
# one must sum the municipality variables annually across all companies serving 
# that municipality (AGS). the AGS, contained in variable wvg_EF2, acts as the 
# ID variable of the municipalities. there is no equivalent for this structure 
# in the energy panel.

# how many observations are there per year?
data_w2_0716$wvu[is.na(data_w2_0716$wvu)==TRUE] <- 0
data_w2_0716$wvg[is.na(data_w2_0716$wvg)==TRUE] <- 0
data_w2_0716$wga[is.na(data_w2_0716$wga)==TRUE] <- 0

# ... for water supply companies 
# (attention, not clear ~ analogous to the operating level)
addmargins(table(data_w2_0716$wvu,data_w2_0716$jahr,useNA="ifany"))
# ... for supplied municipalities 
# (attention, not unambiguous ~ analogous to the operating level)
addmargins(table(data_w2_0716$wvg,data_w2_0716$jahr,useNA="ifany"))
# ... for water extraction plants 
# (attention, not unambiguous ~ analogous to the operating level).
addmargins(table(data_w2_0716$wga,data_w2_0716$jahr,useNA="ifany"))

data_w2_0716 = data_w2_0716 %>% select(EF1, jahr, wvg, wga, wvu, wvg_EF2, 
                                       everything())
dim(data_w2_0716)

# remove all empty fields, since Stata does not handle empty strings.
data_w2_0716[data_w2_0716=="."] <- NA
data_w2_0716[data_w2_0716==""] <- NA

#==============================================================================
# 4.  Save the final data set
#==============================================================================

# export data
# -----------------------
# this is the complete dataset that combines the individual statistics 'wvu', 
# 'wvg' and 'wga' which combines and covers the years 2007-2016.
saveRDS(data_w2_0716,paste0(neudatenpfad, "/", 
                            "4193_Wasser_wvu_wvg_wga_2007-2016.rds"))

#==============================================================================
# End of file
#==============================================================================

