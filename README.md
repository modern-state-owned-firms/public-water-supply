Research project

# 'Modern state-owned firms' performance: An empirical analysis of productivity, market power, and innovation'
financed by the German Science Foundation (DFG) | 2019-2022

Authors of the following codes: Greta Sundermann (@greta.sundermann), Caroline Stiel (@cjstiel)

All codes here provided are open source. All codes are licenced under the [MIT License](https://spdx.org/licenses/MIT.html) expanded by the [Commons Clause](https://commonsclause.com/). All data sets are licenced under the [Attribution-NonCommercial-ShareAlike 4.0 International](https://creativecommons.org/licenses/by-nc-sa/4.0/).

## Data preparation: Erhebung der öffentlichen Wasserversorgung (EVAS 32211)

In this step, we prepare the data on public water supply by merging the following input files: 

|No.| data set | Link to the variable list |
|---|----------|---------------------------|
1|_Wasserversorgungsunternehmen 2007_ provided by [EVAS 32211](https://www.forschungsdatenzentrum.de/de/umwelt/wasserversorgung)|[Link](https://www.forschungsdatenzentrum.de/sites/default/files/wasserversorgung_1998_2001_2004_2007_dsb_wasserversorgungsuntern.pdf)|
2|_Wasserversorgungsunternehmen 2010_ provided by [EVAS 32211](https://www.forschungsdatenzentrum.de/de/umwelt/wasserversorgung)|[Link](https://www.forschungsdatenzentrum.de/sites/default/files/wasserversorgung_ab_2010_dsb_wasserversorgungsuntern.pdf)|
3|_Wasserversorgungsunternehmen 2013_ provided by [EVAS 32211](https://www.forschungsdatenzentrum.de/de/umwelt/wasserversorgung)|[Link](https://www.forschungsdatenzentrum.de/sites/default/files/wasserversorgung_2016_on-site_dsb.pdf)|
4|_Wasserversorgungsunternehmen 2016_ provided by [EVAS 32211](https://www.forschungsdatenzentrum.de/de/umwelt/wasserversorgung)|[Link](https://www.forschungsdatenzentrum.de/sites/default/files/wasserversorgung_2016_on-site_dsb.pdf)|
5|_Wassergewinnungsanlagen 2010_ provided by [EVAS 32211](https://www.forschungsdatenzentrum.de/de/umwelt/wasserversorgung)|[Link](https://www.forschungsdatenzentrum.de/sites/default/files/wasserversorgung_1995_1998_2001_2004_2007_dsb_wassergewinnungsanlage_2.pdf)|
6|_Wassergewinnungsanlagen 2010_ provided by [EVAS 32211](https://www.forschungsdatenzentrum.de/de/umwelt/wasserversorgung)|[Link](https://www.forschungsdatenzentrum.de/sites/default/files/wasserversorgung_ab_2010_dsb_wassergewinnungsanlage.pdf)|
7|_Wassergewinnungsanlagen 2013_ provided by [EVAS 32211](https://www.forschungsdatenzentrum.de/de/umwelt/wasserversorgung)|[Link](https://www.forschungsdatenzentrum.de/sites/default/files/wasserversorgung_2016_on-site_dsb.pdf)|
8|_Wassergewinnungsanlagen 2016_ provided by [EVAS 32211](https://www.forschungsdatenzentrum.de/de/umwelt/wasserversorgung)|[Link](https://www.forschungsdatenzentrum.de/sites/default/files/wasserversorgung_2016_on-site_dsb.pdf)|
9|_Versorgte Gemeinden 2007_ provided by [EVAS 32211](https://www.forschungsdatenzentrum.de/de/umwelt/wasserversorgung)|[Link](https://www.forschungsdatenzentrum.de/sites/default/files/wasserversorgung_1998_2001_2004_2007_dsb_versorgte_gemeinde.pdf)|
10|_Versorgte Gemeinden 2010_ provided by [EVAS 32211](https://www.forschungsdatenzentrum.de/de/umwelt/wasserversorgung)|[Link](https://www.forschungsdatenzentrum.de/sites/default/files/wasserversorgung_ab_2010_dsb_versorgte_Gemeinde.pdf)|
11|_Versorgte Gemeinden 2013_ provided by [EVAS 32211](https://www.forschungsdatenzentrum.de/de/umwelt/wasserversorgung)|[Link](https://www.forschungsdatenzentrum.de/sites/default/files/wasserversorgung_2016_on-site_dsb.pdf)|
12|_Versorgte Gemeinden 2016_ provided by [EVAS 32211](https://www.forschungsdatenzentrum.de/de/umwelt/wasserversorgung)|[Link](https://www.forschungsdatenzentrum.de/sites/default/files/wasserversorgung_2016_on-site_dsb.pdf)|

Start with the R code (master file) ```01_generate_public_water_supply_dataset.r```.

It executes the code ```01a_generate_public_water_supply_dataset.r``` which merges surveys at different levels of observation (water abstraction plants, water supply companies, and supplied municipalities) for the years 2007, 2010, 2013, and 2016: First, we merge the years 2010, 2013, and 2016 for each observation level. Second, we harmonize variables from the year 2007 with those of the following years as the questionnaire slighly changed in 2010 and became more detailed. Thus, variables had to be added or constructed from existing ones. Third, we merge the different observation levels by ```firm ID``` and ```year```.

Output are the data sets ```4193_Wasser_WVU_2007-2016.rds``` (water supply firms), ```4193_Wasser_WVG_2007-2016.rds``` (water abstraction plants), and ```4193_Wasser_WGA_2007-2016.rds``` (municipalities). The data set ```4193_Wasser_wvu_wvg_wga_2007-2016.rds``` combines all three observation levels.

The data sets can be merged with the AFiD data by ```firm ID``` and ```year```. 
